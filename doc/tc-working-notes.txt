tc-working-notes.txt

A blog like file with a running notes in the development of 
the Cape Cod Curling Club Video System.

2024-04-10 tc Initial

Was a liitle late in starting this file.  In middle of picking rack
mounted linux box.  Interrupted to figure out cable lengths and
document prior design work on paper.

2024-04-16 tc at club putting up plywood in the closet

Larry sent a email/picture of system.

1U router

mini-PC

decoder


My response:
   <done> router ok, switch shown needs to be labeled POE.
          2nd Non-POE (from rack) switch for 10.0.0.0 network
          in anticipation of moving compass modem and wifi to rack.
          Need to show 172.16 wifi

My questions:
    A. Put SD cards in cameras?

    B. Scoreboard cameras ... You call out a 4 port video encoder.
       Where does it mount? Do they make single port ? If so, mount it with
       the camera to shorten coax run? 


Building Dwgs:

sheet size from

https://lodicurling.org/index.php/curling/about-curling/curling-basics/the-sheet#:~:text=Curling%20is%20played%20on%20a,line%20called%20the%20%E2%80%9Cteeline%E2%80%9D.

Curling is played on a “sheet” of ice that is 146′ long by 14′ to 15′
wide. Down the center of each sheet of ice is a
“centerline”. Perpendicular to the centerline and 16′ from each end of
the sheet is another line called the “teeline”.

2024-04-21 tc

Created gitlab/dinkumsoftware/cccc
Populated it from private repo cccc-mine.

Captured from Paper Notes (archived)

Video IP formats from Larry: RTP RTSP RTCP H.265 compression

Comcast:
    measured:
         19 mbs UP
        390 mbs Down


Created gizmo:~tc/projects/cccc-private for stuff we don't want the world to see.
Only a local git repo on gizmo at the moment. Probably out to push to the cloud
somewhere.... private gitlab/dinkumsoftware/cccc-private ?

Off to the celtics....

Next:
    set up cccc/hardware:
      mv in cam-but from cccc-mine
        cd cccc-mine ; git reset --HARD head ; mv . ~/keep
      pull from notebook/email

2024-04-22 tc
Celtics won big in game 1 over miami

Made gitlab/dinkumsoftware/cccc-priv

Entered IP addresses and put in dns as *.cccc.dinkumsoftware.com

Next:
    go thru hardware, building paper/repo
    start with cam-but

2024-04-24 tc
Filing, filing, filing...

    
2024-05-07 tc
See cccc-priv/comcast/comcast-notes.txt

2024-05-29 tc

<done> update dns
<done> print labels
<done> relabel warm-room cables
<done> label warm-room monitors/decoders
<moved> move comcast modem
<punt> brief power on/power off test

2024-05-29 tc 3 new warm room monitor cables (use two monitors/sheet)
              rename dining room cables (use 3 monitors)Based on decision to:
    (a) use two monitors/sheet in the warm room:
    (b) use three monitors in dining room

Strung 3 new cables from rack to warm room monitors:

    sheet  color  temp # name
    ----------------------------
    sh1    red    32    MON-S1-WM-R
    sh2    blk    31    MON-S2-WM-R
    sh3    blu    33    MON-S3-WM-R

This forces rename of existing warm room cables:
    MON-S1-WM -> MON-S1-WM-L
    MON-S2-WM -> MON-S2-WM-L
    MON-S2-WM -> MON-S3-WM-L

(b) above forces rename of dining room cables:
    MON-spare-1 ==> MON-DR-2
    MON-spare-2 ==> MON-DR-3

Need to pick IPs for new devices.
Files to change: (URL)
      cccc/network/device-names.txt
      https://gitlab.com/dinkumsoftware/cccc/-/blob/main/network/device-names.txt?ref_type=heads

    cccc/hardware/cabling/cable-runs.ods
    https://gitlab.com/dinkumsoftware/cccc/-/blob/main/hardware/cabling/cable-runs.ods?ref_type=heads

    cccc/hardware/cabling/cable-runs.pdf
    https://gitlab.com/dinkumsoftware/cccc/-/blob/main/hardware/cabling/cable-runs.pdf?ref_type=heads
    

2024-06-01 tc
They wired tested sheet1 with cable tester.

Two bugs in my doc:

    Al:
    You are correct about mis-name.  It should be WR not WM.  I'll change the doc and redistribute.

    PHIL:  change in label needs per above:

        MON-S1-WR-R    MON-S2-WR-R    MON-S3-WR-R
        MON-S1-WR-L    MON-S2-WR-L    MON-S3-WR-L
        MON-DR-1          MON-DR-2    MON-DR-3


    > One error in Tom's wiring document, they pulled a black cable instead of blue for 33 / MON-S3-WM-L
I'll adjust the doc.

Need to update DNS.
 
diff --git a/network/device-names.txt b/network/device-names.txt
index 7bc3131..cd818c4 100644
--- a/network/device-names.txt
+++ b/network/device-names.txt
@@ -17,0 +18 @@ IF YOU CHANGE THIS FILE...
+2024-06-01 tc two bug fixes: WM->WR ; MON-S3-WR-L blk not blue
@@ -29 +30 @@ Naming scheme:
-     |   |   |            WM    Warm Room end
+     |   |   |            WR    Warm Room end
@@ -70,2 +71,2 @@ MON-SKP-S1-WR       172.16.1.34
-MON-S1-WM-R         172.16.1.35
-MON-S1-WM-L         172.16.1.36
+MON-S1-WR-R         172.16.1.35
+MON-S1-WR-L         172.16.1.36
@@ -85,2 +86,2 @@ MON-SKP-S2-WR       172.16.2.34
-MON-S2-WM-R         172.16.2.35
-MON-S2-WM-L         172.16.2.36
+MON-S2-WR-R         172.16.2.35
+MON-S2-WR-L         172.16.2.36
@@ -103,2 +104,2 @@ MON-SKP-S3-WR       172.16.3.34
-MON-S3-WM-R         172.16.3.35
-MON-S3-WM-L         172.16.3.36
+MON-S3-WR-R         172.16.3.35
+MON-S3-WR-L         172.16.3.36

Manually delete in godaddy:
    MON-S1-WM.cccc
        S2
        S3
    MON-DR

Add to insert and import;   
    +MON-S1-WR-R         172.16.1.35
    +MON-S1-WR-L         172.16.1.36

    +MON-S2-WR-R         172.16.2.35
    +MON-S2-WR-L         172.16.2.36

    +MON-S3-WR-R         172.16.3.35
    +MON-S3-WR-L         172.16.3.36

    MON-DR-1            172.16.4.1
    MON-DR-2            172.16.4.2
    MON-DR-3            172.16.4.3

2024-06-13 tc

Found a modified cable-runs.odt recovered by spreadsheet.  Appears identical
to what's in git .... I put the editted version in git.

2024-06-14 tc
At club.  Identified and Labeled sheet 3 ice cameras an 9mm

Need to work thru some or all of paper action items from last visit.


<done> move comcast modem
<issued> get dns on 172... rocky ?
<issued> move ntp to rocky.

Put in cname ntp.cccc.dinkumsoftware.com     
<issued> verify ntp running on vsrvr-3
<done> cable-runs.ods ... removed sheet name at top and page # at bottom.

2024-06-18 tc

2nd day at club.  Tidied up the rack wiring.  Put in a 20A extension so
rack can be pulled out the door.

<done> remind peter to preserve this capability when puts gutter on
to cover wire.

Tried to move comcast gear, but need to move 500 lb front desk.
Need help.

Tidy up POE switch
    label it
    get port management ip working
    tidy up power cord
    figure out connection to 2nd switch

SW-POE    172.16.1.2

<done> create artwork/red-book-cover.txt with TOC

I think the two ports in upper right corner of switch are non-POE,
maybe management ports.
I see traffic it on via wireshark.
Switch is suppose to be 10.0.0.1 and have dhcp server.
Didnt respond to a ping or web browser. Need to find it's current IP address.
Can:
    use angry IP scanner, but it will take a while
    Didn't find it, but switch must be set to dhcp as
        it is boardcasting dhcp discovery packets.

could Do a factory reset, but it says have nothing plugged in.
I could ignore that and/or unplug all the patch cords.

See if I can bring up dhcp server on gizmo.
  https://ubuntu.com/server/docs/how-to-install-and-configure-isc-dhcp-server

  sudo apt install isc-dhcp-server

  You will probably need to change the default configuration by editing
  /etc/dhcp/dhcpd.conf to suit your needs and particular configuration.

      Set it to: 172.16.6.* # DHCP from gizmo (tom's laptop)

  You also may need to edit /etc/default/isc-dhcp-server to specify the
  interfaes dhcpd should listen to.

  INTERFACESv4="eth4"
  After changing the config files you need to restart the dhcpd service:
    sudo systemctl restart isc-dhcp-server.service


# CCCC service
#subnet 172.16.0.0 netmask 255.255.000.000 {  # no-workee
subnet 0.0.0.0 netmask 255.255.255.255 {      # no-workee

from var/log/syslog
No subnet declaration for enxa0cec806c1bf (10.0.0.2).
Jun 18 14:36:05 gizmo dhcpd[21111]: ** Ignoring requests on enxa0cec806c1bf.  If this is not what
Jun 18 14:36:05 gizmo dhcpd[21111]:    you want, please write a subnet declaration
Jun 18 14:36:05 gizmo dhcpd[21111]:    in your dhcpd.conf file for the network segment
Jun 18 14:36:05 gizmo dhcpd[21111]:    to which interface enxa0cec806c1bf is attached. **
Jun 18 14:36:05 gizmo dhcpd[21111]: 

Better.
# CCCC service
subnet 0.0.0.0 netmask 0.0.0.0 {
  range 172.16.6.2 172.16.6.254 ;
}

Offering 172.16.6.2 ... but switch not taking it.
I don't get it.  Just factory reset the thing.

set gizmo to dhcp

Catalyst 2960-S Switch Getting Started Guide
Resetting the switch page 28
Press and hold the Mode button. The switch LEDs begin blinking after about
3 seconds. Continue holding down the Mode button. The LEDs stop blinking after
7 more seconds, and then the switch reboots.

The switch now behaves like an unconfigured switch. You can enter the switch IP
information by using Express Setup as described in the “Running Express Setup”
section on page 4.
Accessing Help Online

page 4:
During Express Setup, the switch acts as a DHCP server.

Power the switch 

Approximately 30 seconds after the switch
powers on, it begins the power-on self-test
(POST), which can take several minutes to
complete.
During POST, the SYSTEM LED blinks
green, and the RPS, STATUS, DUPLEX, and
SPEED LEDs turn solid green.
When POST is complete, the SYSTEM LED
remains solid green, and the other LEDs go
off. The exception is the STACKMASTER
LED, which remains solid green if the switch
is stackable and is acting as stack master.
Before proceeding to the next step, wait until
POST is complete. This might take up
to 5 minutes after the switch is powered on.

Press and hold the Mode button until all of the
LEDs above the Mode button turn solid green.
You might need to hold the button for 3 or
more seconds.

Release the Mode button after all of the LEDs
above the Mode button turn solid green. (The
RPS LED remains off on some switch models,
and is not present on all models.)
The switch is now in Express Setup mode.
Before proceeding to the next step, make sure
that all of the LEDs above the Mode button
are solid green.

Enter the IP address 10.0.0.1 in a web browser
and press Enter.
When prompted, enter the default password,
cisco.
Note
The switch ignores text in the
username field.
The Express Setup window appears.

======
It worked without powering off/on.
Got a 10.0.0.2 IP


&&&&&&
for install.txt

username: <leave blank>
password: cisco

======

click web console link.  failed 400
futzed around with mode/power/ which port on switch, gizmo static/dhcp
finally got it back by:
    gizmo to dhcp
    plug in console port (got an ip)
    http: 10.0.0.1 works
    http: Web Console: gets 404 not found http://10.0.0.1/homepage.htm

I can telnet to it.

Help:
    CCO at www.cisco.com - Cisco Connection Online, including the Technical Assistance Center (TAC).
    tac@cisco.com - e-mail the TAC.
    1-800-553-2447 or +1-408-526-7209 - phone the TAC.
    cs-html@cisco.com - e-mail the HTML interface development group.

I powered off/powered on
and I see switch asking for ip address.

help mode button for 3+ seconds.
now gizmo gets ip via dhcp.... remember to turn gizmo ethernet back on after switch goes down!
web management link is still busted.

sigh .... time for jakes.
I need help from cisco.
email to
    cs-html@cisco.com - e-mail the HTML interface development group.
    tac@cisco.com - e-mail the TAC.

sigh automated response to create account.
Did so.  No free tech support, they want money.
The switch as been EOL'ed as of 2019.

2024-06-19 tc

Need to buy some stuff:
    switch for under desk
    corkboard
    map tacks
    cable hanger

<done> buy 1U linux box.  In mean time duplicate services on gizmo
       dhcp
       dns
       ntp
       web server? ftp ?
       ssh ?

done. single amazon order.

<issued> ok, punt on setting the management port of sw-poe
       label it with sw-poe network/ management ip
       tidy up power cord
       document what should work (copy for working notes) to:
         hardware/switches/switch-cisco-poe-48port/
            switch-cisco-poe-48port.txt
            install.txt
            make a gitlab issue

<issued>put the non-poe switch in:
    wire it to sw-poe
    label it sw-non-poe
    gather manuals
    try to set management IP.

2024-06-20 tc
Got rebound covid.  That sure changes plans.

2024-06-21 tc
Need to buy a linux box.  Initial uses:
     routing VideoNet <-> Club net
     dns, ntp, dhcp, http
     vpn host

Requirements:
    1U
    2 ethernet ports
    Ubuntu
    maybe extra disk (video after all)

Choices:
x     $470 Supermicro | SuperServer 1019C-HTN2 LGA 1151
          no os.  no data sheet on line

      $377 wired zone
      Supermicro SYS-5015A-H 1U Barebone Dual Intel Atom 330 Processor
      Up to 2GB SATA 2 Gigabit Ethernet
      Not sure how much disk space

      $460 savemyserver.com
      USED Dell PowerEdge R630 Server
      No disks

      $1955 
      https://www.thinkmate.com/system/superserver-511r-ml
      Supermicro SuperServer
      511R-ML - 1U - 3x 2.5" SATA - 2x M.2 - Dual 1GbE - 350W
      Ubuntu 22.xx
      	Thinkmate Sales
        tmsales@thinkmate.com
        1-800-371-1212
      assigned sales person.
      Amir Golparvar Amirg@thinkmate.com 1-800-371-1212 x4205

      $1294.87  https://mitxpc.com/products/1u2e-c252
      ubuntu
      PHONE 510-226-6883
      FAX   510-226-6883
      EMAIL sales@mitxpc.com

!!!!!!!!!!!!!!!!!!!! Da winner !!!!!!!!!!!!!!!
    $896  https://www.metservers.com/dell-emc-poweredge-r440-4-bay-lff-1u-rackmount-server
     Dell PowerEdge R440
      Used
      ubuntu
   John Ho P: 510-226-6883 (x103) | www.mitxpc.com
   MITXPC, Inc. | 45437 Warm Springs Blvd, Fremont, CA 94539

<done> document this in git.

Sigh... I ordered from the wrong damn company that doesn't put an OS on it.
I verbally cancelled order #MS-2715 and was told I'd get an email in an hour or two.
  support@metint.com
  972 798 1788

The winner is www.metservers.com


Back to club solo.

tidyed up the sw-poe
al put sw-non-poe in rack

switch doesn't work.
sigh....
i have sw-non-poe plugged into sw-poe.
    works with gizmo into sw-poe
    no workee with gizmo into sw-non-poe

Gather the documentation and factory reset it.
I did so.

    all the port lights turned green and stayed green.
    only a single cable to sw-poe was plugged.
    waited a long time and they stayed green.

moved over the gizmo cable and it appears to work.

192.168.0.239 pings, but got similar error:
This site can’t be reached.

Leave it for now.
<moved> netgear switch:
       label it
       change the management port to sw-non-poe 172.16.0.201
       put it in dns
       

2024-06-22 tc at the club

went to string the 2 ethernet accross the top of the closet.
I had 25' ... I needed 30'
tried (unsuccessfully) to crimp them myself and failed.

Buy them.

2024-06-24 tc

at club just before big work party.
Larry got covid.  I need to step up to camera alignment.

Need to bring up the video net wifi.
I screwed up in IP assignment, the wifi side needs to be on another subnet.
All the cameras are set to 255.255.255.255
I think it will work leaving them as is
Make gizmo 172.16.0.1

Set it's netmask appropriately.

Using:
    172.16.{0,1,2,3,4,5}.*/      
            spare 6,7
    /20 (16 + 8 - 3)    11110000 => 240
    255.255.240.0


wifi
   LAN 172.16.0.3        255.255.240.0      255.255.374.0 gw 172.16.0.1

   WIFI net 172.16.9.0/20  255.255.240.0
   dhcp 172.16.9.100-254     /20      

<punt> adjust channel # if collides.
my wifi app just lists ".." for change for cape cod curling club.

it's brought up as access point, but no way to set DHCP
bring up dhcp on gizmo.

Go back to netmask of 255.255.255.255
tried, and wifi unit complained.

gizmo dhcp didn't work.

Try ebba modem with prior network settings. 255.255.24.0

Factory reset it.
Default IP is 192.168.2.1

check network mask
    11110000  /22
    255.255.240.0

Config it:
    LAN:    172.16.0.3 /21 255.255.240.0
    DHCP:   172.16.0.150 - 172.16.0.175
    user/passwd: None

<issued> set a password
seems to work

There was like a 20 second lag on the video.
Might be because multiple people looking at video.
<issued> chase this down


The cameras weren't configured the same.
Some had anonymous viewing...some didn't
I made them all anonymous.
<punt> do we want anonymous viewing? want an "oper" user with password?

2024-06-25 tc
Moved comcast stuff/phone to closet.


2024-06-26 tc

<issued> cable-runs.ods   change mon-dr-{1,2,3} to VSRVR-{1,2,3}
       negative.  make vsrvr'* cnames in device-names

moved ethernet cable on vsrvr-3 and it pinged.

tried the same thing on vsrvr-2 and it didn't ping.
don't think I had port lights on vsrvr-2
<issued> put electrical tape on unused vsrvr ports.
<punt> document the frigging connector.

2024-06-28 tc

<done> see email.  Bad warm room cable
<issued> see email.  router/vpn up

Need to order some 2' patch cables to go from punchdown to non-poe switch.

non-poe devices:
sheet:
    warmroom end        blk    red    blu   grn   yel   wht
        mon-skp         2      2      2
        mon-wr-r
    scoreboard end
        mon-skp         2      2      2
        mon-wr-l

    dining room:                            3
        vsrvr

    rack:
        router                                           1
        rocky                                            1
        sw-1
        sw-club  already in place
        sw-poe to sw-non-poe                             1

order 4 sets o 6

2024-06-29 tc

Just a note, public users can't see issues in gitlab.


2024-06-30 tc @ club

Club network <done> document/dns this.    X.club.cccc.dinkumsoftware.com
router    10.1.10.1

2024-07-01 tc At the club.

Labeling comcast....
    wan: gateway: 73.47.184.1
    dns: 75.75.75.75
         75.75.76.76

    Lan    10.1.10.1/24
    dhcp       10.1.10.  2 -> 225

        user: cusadmin
        passwd: <root std> sans !
        
        wifi ssid: CurlingClub
        Technicolor Model:CGA4332COM Serial Number:348968234025001062

        Port forwarding:
             Compressor    TCP/UDP port: 8000 10.1.10.56
             Wireguard VPN UDP     port:51820 10.1.10.222

        Remote management:
           https: 8181  73.47.184.145
<done> record above in hardware/comcast/comcast.txt

Label comcast:
    comcast 10.1.10.1 user: cusadmin
    passwd: <root std> sans !

Put club net in dns: xxx.club.cccc.dinkumsoftware.com from
cccc-private/network/dns-zone-file-insert.txt

    comcast     10.1.10.1
    printer     10.1.10.2
    compressor  10.1.10.56
    npv         10.1.10.222
    Put them in imported template, but not device_names.txt

sigh.... printer is dhcp and changes.
Change to static 10.1.10.2

comcast/club net is done

On to sw-non-poe:
x    move it in rack
x    get manual
     name in dns, device-names.txt
     label

2024-07-02 tc @club

Power:
Phil and Chuck chased down breaker for overhead ice.
It's in a breaker panel in compressor room.

It's one leg of 480V circuit.
One of the other legs goes to compressor lights.
The other leg is unknown.
It is labeled TRANS.

Thermal:
Put thermometer on top of rack. (about 30min settle time)
    Room Temp (in front of TV)    72 deg
    Rack Top Temp                 


url:
https://www.youtube.com/@capecodcurlingclub

Put in doc/HOWTO-viewing.txt

collected monitor manuals/remotes.


2024-07-03 tc

Cleaned up the red book.

updated hardware/vsrvr/vsrvr.txt

At club.

Clean up sw-non-POE and sw-/

<done> netgear switch:
x       label it
<issued> later   change the management port to sw-non-poe 172.16.0.201
x       put it in dns

wifi:
  reconfig as access point 172.16.03/32
  dhcp: 172.16.6.2-254        DHCP from wifi
  dns:  

  add passwd to ssid         
  add passwd to admin
  change search path to cccc.dinkumsoftware.com
  label
  document in wifi.txt

<issued> there is no route from VideoNet to the world.


Switched to access point.
Bricked the modem.... sigh ... now I have no DHCP

Somehow bricked gizmo's ethernet using ifconfig to set IP address.
Had to power off.

https://www.tecmint.com/ifconfig-command-examples/#:~:text=How%20to%20Assign%20an%20IP,IP%20address%20to%20interface%20eth0.


sudo ifconfig enxa0cec806c1bf 172.16.0.10 netmask 255.255.255.255 broadcast 172.16.255.255
sudo route add gw 172.16.0.1
sudo route add -net 172.16.0.0 netmask 255.255.255.255 gw 172.16.0.1
sudo ifconfig enxa0cec806c1bf up

https://www.tecmint.com/ip-command-examples/

     ip link show
     ip link show enxa0cec806c1bf
sudo ip addr add 172.16.0.10/32 dev enxa0cec806c1bf
sudo ip link set dev eth2 up # or down

ip link set dev <eth> up # hung as well.
I'm guessing some interact with gnome network manager.
Just use that.

2024-07-04 tc
I think the above woes were using a netmask of 255.255.255.255 instead
of 255.255.0.0   duh

Rocky arrived.
See hardware/linux-server/{linux-server,install.txt}

Configure as:
    1. router
    2. dhcp
    3. dns
    4. ntp ?

Back to club ... I didn't have a USB keyboard

Get the wifi back up

<done> copy this to dinkum/network/doc/HOWTO-configure-ethernet-port.txt
In settings, set:
    Settings->Network->button settings->IP4
    Manual
        Addresses 17216.0.10 255.255.0.0 gw:blankd
        DNS: non-automatic. 8.8.8.8
        Routes: non-Automatic 172.16.0.0 255.255.0.0 172.16.0.1
    Apply
    Cancel
    Flick Power OFF/ON to be sure


c@gizmo:~/projects/cccc/hardware/server-linux$ ifconfig enxa0cec806c1bf
enxa0cec806c1bf: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.16.0.10  netmask 255.255.0.0  broadcast 172.16.255.255
        inet6 fe80::5235:c93e:229b:68d6  prefixlen 64  scopeid 0x20<link>
        ether a0:ce:c8:06:c1:bf  txqueuelen 1000  (Ethernet)
        RX packets 55497  bytes 10580027 (10.5 MB)
        RX errors 0  dropped 644  overruns 0  frame 0
        TX packets 160  bytes 25887 (25.8 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 

        
tc@gizmo:~/projects/cccc/hardware/server-linux$ route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
169.254.0.0     0.0.0.0         255.255.0.0     U     1000   0        0 enxa0cec806c1bf
172.16.0.0      0.0.0.0         255.255.0.0     U     100    0        0 enxa0cec806c1bf
172.16.0.0      172.16.0.1      255.255.0.0     UG    100    0        0 enxa0cec806c1bf
     
    
Life is good, back to the modem

Do work in hardware/wifi/install.txt

<moved> move wifi config to another repo... It has password in it
       change wifi/install.txt
       check other devices with password

2 wifi problems:
    1) It insists that dhcp is in /24 address.
    Won't let me change it to /16

    2) No modern security protocols
       Club uses WPA/WPA2-Personal

<moved> buy a new wifi unit ... sigh


2024-07-05 tc

thermal after all night with tile off 11:20 :  73 degree

Brought the wifi back up as a router with split network.

AP didn't work, I'm guessing because no DHCP server on 172 net.
Giving out a 192 address.

I need to figure this out.

Best choice:
use as router with dhcp
Problem is need separate network for wifi clients (and wifi dhcp)
    Can either break up the 172 net --or--
    use a 10. or 192. network.

Now:
    172.16.0.0/16 255.255.255.255
    Using:
        172.16.{0,1,2,3,4}.*

    dhcp:
        172.16.{5,6}.*   rocky, wifi

When back to old configuration and it seems to be working:
    lan:    172.16.0.3   255.255.240.0
    dhcp:   172.16.0.{150-175}

240b10 => 11110000

Can use 172.17.0.0/24  2-254
172.16.0.0/12 IP addresses: 172.16.0.0 – 172.31.255.255

The belkin is buggy.  Swap in the other dinkum wireless unit.
It's no good either.  Buy a new unit.
<done> move wifi/belkin stuff to wifi/yesterdays-news/belkin-wifi
<done> move wifi config to another repo... It has password in it
       change wifi/install.txt
       check other devices with password
<done> buy a new wifi unit ... sigh

Walmart Linksys AC1200 (E5400) Ordered for delivery mon jul 8


https://www.walmart.com/ip/Linksys-AC1200-Dual-Band-WiFi-5-Router-with-Easy-Setup-Black/195715927?wmlspartner=wlpa&selectedSellerId=0&wl13=1906&gclsrc=aw.ds&adid=22222222277195715927_117755028669_12420145346&wl0=&wl1=g&wl2=c&wl3=501107745824&wl4=pla-394283752452&wl5=9002090&wl6=&wl7=&wl8=&wl9=pla&wl10=8175035&wl11=local&wl12=195715927&veh=sem_LIA&gclsrc=aw.ds&gad_source=1&gclid=Cj0KCQjws560BhCuARIsAHMqE0F6-xqLXXJssuF6arNucNN9BzULbOv_wKy41kbu5MqniS_1xkW0j1IaAvImEALw_wcB

On to rocky.
sigh ... plugged in usb keyboard and hdmi monitor ... nothing
sent off email.

clean up git.

2024-07-06 tc

Sent email off to server manufactorer on how to get initial login.
Basically need a VGA monitor, not HDMI
See if I've got on.

No real help from Ubuntu.

Finish putting in gitlab issues.
move todo's ==> issues

The club isn't up.
Write an email to peter, cc larry, chuck, phil, al
================================
Table of contents:
    router being powered down
    vsrvr-spare being powered down
    dining room monitors dead
    warm room monitors have issues


1) I'm going to power down and move the router to the rack in the next day or two.  I understand it's required for streaming Utube.  Larry:  It it doesn't self start, you need to:
   a. tell me what I have to do to restart it
   b. come in and do it while I'm at the club so I can write it down.

2) Likewise VSRVR-spare will be powered down.  I'm guessing (I should know) that you are using it for Utube feed.  If it doesn't self start:
   a. tell me what I have to do to restart it
   b. come in and do it while I'm at the club so I can write it down.

I think in general it's untenable that you are the only one that knows how to start stuff.

3) The dining room monitors show no content. VSRVRs are pingable. Nothing on any of HDMI 1,2,3 on the monitors.  I suspect the application software needs to be started. (see #2).  This means hauling a keyboard, mouse, monitor to the dining room.  Untenable long term.  Near term either haul all the gear there and hope nobody powers it down.  It might be doable to put an ssh server on Windoze and start it from the command line.

4) The warm room monitors need adustment.
    a. unless you changed it, several of images need to be flipped left/right.
    b. Both peter and I agree we should offset the buttons in the pair of monitors to differentiate scoreboard end from warm room end.  How does this get solved?  Do we change the area of view in camera? does that screw up utube feed? Can we do it in decoder?  Please advise.
    c. Various amounts of "curvature" in images
    d. New Issue.  With ice down, white stripes in all the images.  This could be a hard one.

===============================


I wanted to plug a USB cable into rocky and see if supported a serial login screen.  I didn't have the right cable with me.  Need AM-AM or adapters for C.

Brought VGA cables, but can't get to back of monitor very easily.

Hooked up a vga monitor.
    login: user1 password
    sudo         password

ethernet's not configure
no sshd

Get it in the rack.
rocky is in the rack.
Get ethernet ports configured and hopefully dhcp up.
Check the BIOS to see if it autostarts.

interrupt # 1264
Larry came in.

=====
router access is root <std password>
    1. web browse to it.
    2. ssh to it.

vpn needs wire guard client.

Use hdmi 1 on dining room monitors.
=====

remote desktop should be running on vsrvr-<n>
install it and document in doc/client-reqd-software.txt
change of plans.... use vnc

brought up dhcp on router 172.16.7.*

One or two of the score board cameras aren't responding.
that got left with a dhcp address.  Larry fixed by
depowering, talking to their dhcp address, setting proper
static ip.

2024-07-07 tc

Need to finish up the expenses

Buying tools.  Need

Tool Box/Bag (note hammer is 11 3/8" long)
x wire cutters
x wire stripper
x pliers
x screw drivers
x    right angle
x    handful of straight/phillips
x    a big one with interchanable shafts
x chan lock
drill and bits ?

cable mounts
eye hooks
cup hooks

2024-07-07 tc

@club
Mounted two power units at bottom of rack.

Moving router to rack.  Of course larry was
working remotely when I arrived.

Trying to get a boot screen from router.
Had keyboard in front and hdmi in back and got blank screen.

Plugged into usb C on back from gizmo and got syslog:

Jul  7 14:30:01 gizmo kernel: [ 5152.947534] usb usb2-port1: Cannot enable. Maybe the USB cable is bad?
Jul  7 14:30:01 gizmo kernel: [ 5152.947660] usb usb2-port1: attempt power cycle
Jul  7 14:30:06 gizmo kernel: [ 5157.351513] usb usb2-port1: Cannot enable. Maybe the USB cable is bad?
Jul  7 14:30:10 gizmo kernel: [ 5161.427438] usb usb2-port1: Cannot enable. Maybe the USB cable is bad?

Just try to reboot it.

got boot screen on hdmi and login on keyboard

<todo> couldn't figure out how to get to BIOS.
       send email

<todo> ssh-copy-id didn't survive reboot

power it down.
<todo> had to push router the button to power it back up.

2024-07-08 tc

Tried to return the comcast boxes and failed.  Sent internal email.

We got the comcast service speed upgraded and signed a 2 yr contract back in early May I think.
It included two services we don't want:

    connection pro
    wifi pro

=============

The installer left two unopened boxes of hardware. He told us (incorrectly as it turns out) that we have "free" service for 6 months and then additional fees kick in.  He said to return the two boxes to local comcast office.

Well I tried to return them.  They wouldn't take the boxes as it's a business account.  After 30 minutes on phone with comcast it appears we are already paying for these unused services, but ... if we cancelled it would "break the agreement" and seriously up the price.  Is it any wonder that everybody hates comcast.

I labeled the boxes comcast and put in basement with rest of video gear.

=============

Send email about powering on automatically to vsrvr and router vender.

<todo> vsrvr-3 videoNet RJ45 is in different connector.
<todo> can't log in to decoder-spare. record the admin/root username of all devices.
<todo> 3 screws missing back of vsrvr-spare
<todo> mon-dr-3 ethernet plugged into a different port (not one next to power cord)



